//
//  main.cpp
//  Education
//
//  Created by Gordiy Rushynets on 3/5/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Education
{
protected:
    string name;
    string address;
    string birth_year;
public:
    void set_name(string ed_name)
    {
        name = ed_name;
    }
    
    void set_address(string ed_address)
    {
        address = ed_address;
    }
    
    void set_birth_year(string ed_birth_year)
    {
        birth_year = ed_birth_year;
    }
    
    string get_address()
    {
        return address;
    }
    
    virtual istream &input(istream &in)
    {
        return in >> name >> address >> birth_year;
    }
    
    friend istream &operator >> (istream &in, Education &ed)
    {
        return ed.input(in);
    }
    
    virtual ostream &output(ostream &out)
    {
        return out << name << ' ' << address << ' ' << birth_year;
    }
    
    friend ostream &operator << (ostream &out, Education &ed)
    {
        return ed.output(out);
    }
};

class School : public Education
{
private:
    int school_number;
    int cout_pupils;
public:
    friend ostream &operator << (ostream &out, School &school)
    {
        return out << school.school_number << ' ' << school.cout_pupils;
    }
    
    friend istream &operator >> (istream &in, School &school)
    {
        return in >> school.school_number >> school.cout_pupils;
    }
    
    void set_school_number(int school_num)
    {
        school_number = school_num;
    }
    
    void set_count_pupils(int number_pupils)
    {
        cout_pupils = number_pupils;
    }
    
    int get_count_pupils()
    {
        return cout_pupils;
    }
    
    int get_school_number()
    {
        return school_number;
    }
};

class University : public Education
{
private:
    int acredetation;
    int count_faculty;
public:
    friend ostream &operator << (ostream &out, University &uni)
    {
        return out << uni.acredetation << ' ' << uni.count_faculty;
    }
    
    friend istream &operator >> (istream &in, University &uni)
    {
        return in >> uni.acredetation >> uni.count_faculty;
    }
    
    void set_acredetation(int ed_acredetation)
    {
        acredetation = ed_acredetation;
    }
    
    void set_count_faculty(int num_faculty)
    {
        count_faculty = num_faculty;
    }
    
    int get_acredetation()
    {
        return acredetation;
    }
    
    int get_count_faculty()
    {
        return count_faculty;
    }
};

void sort_by_address(Education **education, int count)
{
    for(int i = 0; i < count - 1; i++)
    {
        for(int j = 0; j < count - i - 1; j++)
        {
            if(education[j]->get_address() < education[j+1]->get_address())
            {
                std::swap(education[j], education[j+1]);
            }
        }
    }
}

void print_school_max_pupils(Education **education, int school_size)
{
    int max = 0;
    int max_index = 0;
    
    School *school = new School[school_size];
    
    for(int i = 0; i < school_size; i++)
    {
        school = dynamic_cast<School*>(education[i]);
        if(school[i].get_count_pupils() > 4)
        {
            if(max < school[i].get_count_pupils())
            {
                max_index = i;
                max = school[i].get_count_pupils();
            }
        }
    }
    cout << "School with max count pupils number: " << school[max_index].get_school_number() << " count pupils: " << school[max_index].get_count_pupils() << endl;
}

void print_acredetation(Education **education, int acredetation, int count)
{
    University *univer = new University[count];
    
    for(int i = 0; i < count; i++)
    {
        univer = dynamic_cast<University*>(education[i]);
        if(univer[i].get_acredetation() == acredetation)
        {
            cout << univer[i] << endl;
        }
    }
}

int main(int argc, const char * argv[]) {
    int count_education;
    
    // path to file
    ifstream fin_education("/Users/gordiy/cpp/Education/Education/education.txt");
    fin_education >> count_education;
    
    Education **education = new Education*[count_education];
    int reply;
    
    cout << "1 is school 2 is University or Coladge" << endl;
    
    for(int i = 0; i < count_education; i++)
    {
        fin_education >> reply;
        switch (reply) {
            case 1:
                education[i] = new School;
                fin_education >> *education[i];
                break;
            
            case 2:
                education[i] = new University;
                fin_education >> *education[i];
                
            default:
                break;
        }
    }
    
    sort_by_address(education, count_education);
    
    for (int i = 0; i < count_education; i++)
    {
        cout << *education[i] << endl;
    }
    
    print_acredetation(education, 4, 2);
    
    for (int i = 0; i < count_education; i++)
    {
        cout << *education[i] << endl;
    }
    
    print_school_max_pupils(education, 3);
    
    for (int i = 0; i < count_education; i++)
    {
        cout << *education[i] << endl;
    }
    
    return 0;
}
